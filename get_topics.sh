#!/bin/bash

# GRE Essay Tester
# Copyright (C) 2021 Lucy Koch-Hyde

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


outputTopics () {
	topicSedCommand='1,\|^<hr /></div>$| d; \|^<h2>See also:</h2>$|,$ d; \%^\(<hr /></div>\|<div class="divider-50">\)$% d; /^$/ d; s/&nbsp;\|<[^>]*span[^>]*>//g; s/^<p>\([^<]*\)<\/p>$/\1/'
	moduleName="$(basename -- "$1" ".elm")"
	cat > "$1" <<- EOF
	module $moduleName
	  exposing
	    ( topics
	    )


	import Topic
	  exposing
	    ( Topic
	    )


	topics : List Topic
	topics =
	EOF
	html="$(curl "$2" | sed "$topicSedCommand" | tr " \n" "|^" | sed 's/\^<div|class="indented">\^/\&/g' | sed 's/\^<\/div>\^/ /g' | sed 's/"/\\"/g')"
	echo "$common" >> "$1"

	for topic in $html
	do
		echo     "  { prompt =" >> "$1"
		arr=( $(echo "$topic" | tr "&" " ") )
		promptLines="$(echo "${arr[0]}" | tr "^" " ")"
		for promptLine in $promptLines
		do
			echo "    \"$(echo "$promptLine" | tr "|" " ")\"" >> "$1"
			echo "    ::" >> "$1"
		done
		echo     "    []" >> "$1"
		echo     "  , direction =" >> "$1"
		directionLines="$(echo "${arr[1]}" | tr "^" " ")"
		for directionLine in $directionLines
		do
			echo "    \"$(echo "$directionLine" | tr "|" " ")\"" >> "$1"
			echo "    ::" >> "$1"
		done
		echo     "    []" >> "$1"
		echo     "  }" >> "$1"
		echo     "  ::" >> "$1"
	done
	echo         "  []" >> "$1"
}

outputTopics src/Issue.elm "https://www.ets.org/gre/revised_general/prepare/analytical_writing/issue/pool"
outputTopics src/Argument.elm "https://www.ets.org/gre/revised_general/prepare/analytical_writing/argument/pool"
