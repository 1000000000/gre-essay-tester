# GRE Essay Tester

A simple web app for practicing for the GRE analytical writing section.
To get it started you will need to load the topic pools which can be done by running the `get_topics.sh` script from the root directory of the repository.
The script itself is super janky so no guarantees it won't break sometime in the future.
Once the script has generated the two topic modules, then you can compile the app with `elm make src/Main.elm`, or run `elm reactor` and navigate to http://localhost:8000/src/Main.elm.
When the page loads, simply click the button corresponding to the type of essay you would like to practice and the app will select an appropriate topic and provide you with a space to write your essay as well as a timer.
When the timer reaches zero, the text area will lock, preventing you from making further changes to what you've written, though you will still be able to read your essay.
Note that this app does not save its state, so if you close or reload the page, any work you've done will be lost and you'll be put back to the main menu.

### Future Plans

* Allow user to set length of timer
* Save state so that closing or reloading the page doesn't obliterate all work done
