{-
GRE Essay Tester
Copyright (C) 2021 Lucy Koch-Hyde

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
-}
module Main
  exposing
    ( ..
    )


import Browser
import Browser.Dom
import Css
import Css.Media
import Html.Styled as Html
  exposing
    ( Html
    )
import Html.Styled.Attributes as HtmlAttr
import Html.Styled.Events as HtmlEvent
import Random
import Task
import Time


import Argument
import Issue
import Topic
  exposing
    ( Topic
    )


type alias Time =
  { minutes : Int
  , seconds : Int
  }


type PauseStatus
  = Paused Int
  | Unpaused Time


type Model
  = MainMenu
  | WritingEssay
    { startingTime : Int
    , pauseStatus : PauseStatus
    , essay : String
    , topic : Topic
    }


type Message
  = SelectIssue
  | SelectArgument
  | GotTopic (Maybe Topic)
  | StartTest Topic Int
  | Tick Int
  | TogglePause
  | PauseTime Int
  | UnpauseTime Int
  | Typing String
  | Noop


allowedTime : Time
allowedTime =
  Time 30 0


textareaId : String
textareaId =
  "essay-box"


renderTime : Time -> String
renderTime { minutes, seconds } =
  let
    width2 : Int -> String
    width2 val =
      if
        val < 10
      then
        "0" ++ String.fromInt val
      else
        String.fromInt val
  in
    width2 minutes ++ ":" ++ width2 seconds


subtractTime : Time -> Time -> Time
subtractTime t1 t2 =
  let
    newSeconds : Int
    newSeconds =
      t1.seconds - t2.seconds
  in
    Time (t1.minutes - t2.minutes + divideBy 60 newSeconds |> max 0) (modBy 60 newSeconds)


divideBy : Int -> Int -> Int
divideBy bottom top =
  if
    ( bottom >= 0
      && top >= 0
    )
      ||
        ( bottom < 0
          && top < 0
        )
  then
    top // bottom
  else
    top // bottom - 1


maybeUniform : List a -> Random.Generator (Maybe a)
maybeUniform list =
  case
    list
  of
    [] ->
      Random.constant Nothing

    x :: xs ->
      Random.uniform x xs
        |> Random.map Just


calculateRemainingTime : Int -> Int -> Time
calculateRemainingTime startingTime currentTime =
  let
    secondsElapsed : Int
    secondsElapsed =
      (currentTime - startingTime)
        |> divideBy 1000
  in
    Time (divideBy 60 secondsElapsed) (modBy 60 secondsElapsed)
      |> subtractTime allowedTime


update : Message -> Model -> ( Model, Cmd Message )
update message model =
  case
    model
  of
    MainMenu ->
      case
        message
      of
        SelectIssue ->
          ( MainMenu
          , Issue.topics
            |> maybeUniform
            |> Random.generate GotTopic
          )

        SelectArgument ->
          ( MainMenu
          , Argument.topics
            |> maybeUniform
            |> Random.generate GotTopic
          )

        GotTopic maybeTopic ->
          case
            maybeTopic
          of
            Nothing ->
              ( MainMenu
              , Cmd.none
              )

            Just topic ->
              ( MainMenu
              , Task.perform (Time.posixToMillis >> StartTest topic) Time.now
              )

        StartTest topic startingTime ->
          ( WritingEssay
            { startingTime =
              startingTime
            , pauseStatus =
              Unpaused allowedTime
            , essay =
              ""
            , topic =
              topic
            }
          , Task.attempt ( \ _ -> Noop ) (Browser.Dom.focus textareaId)
          )

        _ ->
          ( MainMenu
          , Cmd.none
          )

    WritingEssay prev ->
      case
        message
      of
        TogglePause ->
          if
            isPaused prev.pauseStatus
          then
            ( model
            , Task.perform (Time.posixToMillis >> UnpauseTime) Time.now
            )
          else
            ( model
            , Task.perform (Time.posixToMillis >> PauseTime) Time.now
            )

        PauseTime pauseTime ->
          ( WritingEssay
            { startingTime =
              prev.startingTime
            , topic =
              prev.topic
            , essay =
              prev.essay
            , pauseStatus =
              Paused pauseTime
            }
          , Cmd.none
          )

        UnpauseTime unpauseTime ->
          case
            prev.pauseStatus
          of
            Unpaused _ ->
              ( model
              , Cmd.none
              )

            Paused pauseStart ->
              let
                newStartingTime : Int
                newStartingTime =
                  prev.startingTime + (unpauseTime - pauseStart)

                remainingTime : Time
                remainingTime =
                  calculateRemainingTime newStartingTime unpauseTime
              in
                ( WritingEssay
                  { startingTime =
                    newStartingTime
                  , topic =
                    prev.topic
                  , essay =
                    prev.essay
                  , pauseStatus =
                    Unpaused remainingTime
                  }
                , Cmd.none
                )

        Tick currentTime ->
          case
            prev.pauseStatus
          of
            Unpaused remainingTime ->
              let
                newRemaining : Time
                newRemaining =
                  calculateRemainingTime prev.startingTime currentTime
              in
                if
                  newRemaining /= remainingTime
                then
                  ( WritingEssay
                    { startingTime =
                      prev.startingTime
                    , topic =
                      prev.topic
                    , essay =
                      prev.essay
                    , pauseStatus =
                      Unpaused newRemaining
                    }
                  , Cmd.none
                  )
                else
                  ( model
                  , Cmd.none
                  )

            Paused _ ->
              ( model
              , Cmd.none
              )

        Typing newEssay ->
          case
            prev.pauseStatus
          of
            Unpaused remainingTime ->
              if
                remainingTime /= Time 0 0
              then
                ( WritingEssay
                  { startingTime =
                    prev.startingTime
                  , topic =
                    prev.topic
                  , essay =
                    newEssay
                  , pauseStatus =
                    prev.pauseStatus
                  }
                , Cmd.none
                )
              else
                ( model
                , Cmd.none
                )

            Paused _ ->
              ( model
              , Cmd.none
              )

        _ ->
          ( model
          , Cmd.none
          )


isPaused : PauseStatus -> Bool
isPaused pauseStatus =
  case
    pauseStatus
  of
    Paused _ ->
      True

    Unpaused _ ->
      False


viewTextArea : Bool -> String -> Html Message
viewTextArea paused essay =
  Html.textarea
    [ HtmlAttr.spellcheck False
    , HtmlAttr.disabled paused
    , HtmlAttr.id textareaId
    , HtmlAttr.value essay
    , HtmlEvent.onInput Typing
    , HtmlAttr.css <|
      ( if
          paused
        then
          (::) (Css.property "filter" "blur(8px)")
            >> (::) (Css.property "user-select" "none")
        else
          identity
      )
        [ Css.flexGrow (Css.num 1)
        , Css.resize Css.none
        , Css.overflowY Css.scroll
        , Css.fontFamily Css.sansSerif
        ]
    ]
    []


viewFinalDiv : String -> Html a
viewFinalDiv =
  String.lines
    >>
      List.map
        ( Html.text
          >> List.singleton
          >>
            Html.p
              [ HtmlAttr.css
                [ Css.margin Css.zero
                , Css.after
                  [ Css.display Css.inlineBlock
                  , Css.width (Css.px 0)
                  , Css.property "content" "\"\""
                  ]
                ]
              ]
        )
    >>
      Html.div
        [ HtmlAttr.css
          [ Css.flexGrow (Css.num 1)
          , Css.fontFamily Css.sansSerif
          , Css.marginBlockStart (Css.px 1)
          , Css.marginBlockEnd (Css.px 1)
          , Css.border3 (Css.px 1) Css.inset (Css.rgb 224 224 224)
          , Css.property "padding-inline" "1px"
          , Css.Media.withMedia
            [ Css.Media.not Css.Media.print []
            ]
            [ Css.color (Css.rgb 146 149 149)
            , Css.backgroundColor (Css.rgb 224 224 224)
            , Css.overflowY Css.scroll
            ]
          ]
        ]


view : Model -> Html Message
view model =
  case
    model
  of
    MainMenu ->
      Html.div
        [ HtmlAttr.css
          [ Css.margin (Css.em 1)
          ]
        ]
        [ Html.button
          [ HtmlEvent.onClick SelectIssue
          ]
          [ Html.text "Issue Task"
          ]
        , Html.button
          [ HtmlEvent.onClick SelectArgument
          ]
          [ Html.text "Argument Task"
          ]
        ]
    WritingEssay { topic, pauseStatus, essay } ->
      let
        paused : Bool
        paused = isPaused pauseStatus

        testFinished : Bool
        testFinished = pauseStatus == Unpaused (Time 0 0)
      in
        Html.div
          [ HtmlAttr.css
            [ Css.margin (Css.em 1)
            , Css.position Css.absolute
            , Css.width (Css.calc (Css.pct 100) Css.minus (Css.em 2))
            , Css.height (Css.calc (Css.pct 100) Css.minus (Css.em 2))
            , Css.top Css.zero
            , Css.left Css.zero
            , Css.boxSizing Css.borderBox
            , Css.displayFlex
            , Css.flexDirection Css.column
            , Css.fontFamily Css.sansSerif
            ]
          ]
          [ Html.div
            [ HtmlAttr.css <|
              ( if
                  paused
                then
                  (::) (Css.property "filter" "blur(8px)")
                    >> (::) (Css.property "user-select" "none")
                else
                  identity
              )
              [ Css.fontSize (Css.px 16)
              , Css.lineHeight (Css.num 1.5)
              , Css.textRendering Css.optimizeLegibility
              ]
            ]
            [ topic.prompt
              |> List.map (Html.text >> List.singleton >> Html.p [ HtmlAttr.css [ Css.marginBottom (Css.em 0.8) ] ])
              |> Html.div []
            , topic.direction
              |> List.map (Html.text >> List.singleton >> Html.p [ HtmlAttr.css [ Css.marginBottom (Css.em 0.8) ] ])
              |> Html.div [ HtmlAttr.css [ Css.fontStyle Css.italic, Css.margin4 (Css.em 1) Css.zero (Css.em 1) (Css.em 3) ] ]
            ]
          , if
              testFinished
            then
              viewFinalDiv essay
            else
              viewTextArea paused essay
          , Html.div
            [ HtmlAttr.css
              [ Css.Media.withMedia
                [ Css.Media.not Css.Media.print []
                ]
                [ Css.displayFlex
                , Css.flexDirection Css.row
                , Css.justifyContent <|
                  if
                    testFinished
                  then
                    Css.flexEnd
                  else
                    Css.spaceBetween
                ]
              , Css.Media.withMedia
                [ Css.Media.only Css.Media.print []
                ]
                [ Css.display Css.none
                ]
              ]
            ]
            [ Html.button
              [ if
                  testFinished
                then
                  HtmlAttr.css
                    [ Css.display Css.none
                    ]
                else
                  HtmlEvent.onClick TogglePause
              ]
              [ Html.text <|
                if
                  paused
                then
                  "Unpause"
                else
                  "Pause"
              ]
            , Html.div
              [ HtmlAttr.css
                [ Css.fontSize Css.xLarge
                ]
              ]
              [ Html.text <|
                case
                  pauseStatus
                of
                  Paused _ ->
                    "PAUSED"

                  Unpaused remainingTime ->
                    renderTime remainingTime
              ]
            ]
          ]


subscriptions : Model -> Sub Message
subscriptions model =
  case
    model
  of
    MainMenu ->
      Sub.none

    WritingEssay { pauseStatus } ->
      case
        pauseStatus
      of
        Paused _ ->
          Sub.none

        Unpaused remainingTime ->
          if
            remainingTime == Time 0 0
          then
            Sub.none
          else
            Time.every 50 (Time.posixToMillis >> Tick)


main : Program () Model Message
main =
  Browser.element
    { init =
      \ _ ->
        ( MainMenu
        , Cmd.none
        )
    , update =
      update
    , view =
      view >> Html.toUnstyled
    , subscriptions =
      subscriptions
    }
